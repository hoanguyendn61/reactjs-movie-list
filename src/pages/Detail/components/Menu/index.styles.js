import styled from "styled-components";
const StyledShortcutBarScroller = styled.div`
  width: 100%;
  height: 46px;
  display: flex;
  justify-content: center;
  background-color: #fff;
  position: relative;
  top: 0;
  left: 0;
`;
const StyledMenuBar = styled.ul`
  height: 100%;
  background-color: #fff;
  display: flex;
  font-weight: 600;
  flex-wrap: nowrap;
`;
const StyledMenuItem = styled.li`
  padding: 6px 0 4px 0;
  margin-right: 40px;
  color: #000;
  font-weight: 400;
  outline: 0;
  display: flex;
  flex: 0 0 auto;
  position: relative;
  user-select: none;
  border-bottom: ${(props) =>
    props.active ? "4px solid rgba(var(--tmdbLightBlue), 1)" : "none"};
  span {
    padding: 0;
    outline: 0;
    color: inherit;
    font-size: 14px;
    display: flex;
    flex-direction: row;
    flex: 1 1 auto;
    align-items: center;
    position: relative;
    white-space: nowrap;
    cursor: pointer;
  }
`;
export { StyledShortcutBarScroller, StyledMenuBar, StyledMenuItem };
