import {
  StyledHeader,
  CustomBg,
  SingleColumn,
  OriginalHeader,
} from "./index.styles";
import PosterWrapper from "./PosterWrapper";
import HeaderPosterWrapper from "./HeaderPosterWrapper";

export default ({ results }) => {
  return (
    <StyledHeader
      backdropPoster={`https://image.tmdb.org/t/p/w1920_and_h800_multi_faces${results?.backdrop_path}`}
    >
      <CustomBg>
        <SingleColumn>
          <OriginalHeader>
            <PosterWrapper poster_path={results?.poster_path} />
            <HeaderPosterWrapper results={results} />
          </OriginalHeader>
        </SingleColumn>
      </CustomBg>
    </StyledHeader>
  );
};
