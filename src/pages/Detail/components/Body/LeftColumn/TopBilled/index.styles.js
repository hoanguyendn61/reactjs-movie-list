import styled from "styled-components";

const StyledSectionTopBilledCast = styled.section`
  border-top: none;
  padding-top: 0;
  width: 100%;
  display: block;
  padding: 30px 0;
  color: #000;
`;
const StyledCastScroller = styled.div`
  position: relative;
  top: 0;
  left: 0;
  ol.people.scroller {
    overflow-y: hidden;
    overflow-x: scroll;
    margin-left: -10px;
    margin-top: -10px;
    padding-bottom: 10px;
  }
  ol.people {
    list-style-type: none;
    list-style-position: inside;
    margin: 0;
    padding: 0;
    display: flex;
    position: relative;
    top: 0;
    left: 0;
    li {
      min-width: 140px;
      width: 140px;
      a + p {
        padding-top: 10px;
      }
      p {
        padding: 0 10px;
        font-size: 1em;
        margin: 0;
        overflow: hidden;
        text-overflow: ellipsis;
        a {
          font-weight: bold;
          color: #000;
          &:hover {
            color: rgba(0, 0, 0, 0.5);
          }
        }
      }
      p:last-child {
        margin-bottom: 0;
      }
      p.character {
        font-size: 0.8em;
      }
    }
    li:last-of-type {
      margin-right: 0;
    }
    li.filler {
      margin-left: 10px;
      background-color: transparent;
    }
    li.view_more {
      display: flex;
      align-items: center;
      align-content: center;
      margin: 0;
      padding: 0;
    }
  }
`;
export { StyledSectionTopBilledCast, StyledCastScroller };
