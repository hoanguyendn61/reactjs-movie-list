import VideoCard from "./VideoCard";
import { StyledViewMore } from "../index.styles";
import { FaArrowCircleRight } from "react-icons/fa";
export default ({ videos }) => {
  return (
    <>
      {videos.map((item, idx) => {
        return <VideoCard key={idx} videoKey={item.key} name={item.name} />;
      })}
      <StyledViewMore>
        <div className="link">
          <p>
            <a href="#">
              View More &nbsp;
              <FaArrowCircleRight />
            </a>
          </p>
        </div>
      </StyledViewMore>
    </>
  );
};
