import styled from "styled-components";
const StyledDiscussionTr = styled.tr`
  width: 100%;
  background: #fff;
  border-radius: var(--imageBorderRadius);
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 12px 20px;
  color: #000;
  border: 1px solid rgba(var(--lightGrey), 1);
  box-sizing: border-box;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
`;
const StyledData = styled.td`
  font-weight: 300;
  font-size: 0.9em;
  text-align: left;
  padding: 0;
  box-sizing: border-box;
  :first-of-type {
    width: 60%;
    font-size: 1em;
    padding-left: 0;
  }
  :nth-child(2),
  :nth-child(3) {
    text-align: right;
  }
  :last-of-type {
    padding-right: 0;
  }
  :last-child {
    width: 230px;
    padding-left: 20px;
    text-align: right;
    white-space: nowrap;
  }
  p.status {
    padding-right: 40px;
  }
  span.username {
    a {
      color: #000;
    }
  }
`;
const StyledPostInfo = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  box-sizing: border-box;
  flex-wrap: nowrap;
  position: relative;
  top: 0;
  left: 0;
  padding: 0;
  .flex_wrapper {
    width: 100%;
    display: flex;
  }
`;
const StyledAvatarWrapper = styled.div`
  display: flex;
  align-items: flext-start;
  align-content: center;
  span.avatar {
    float: none;
    width: 32px;
    height: 32px;
    display: inline-block;
    box-sizing: border-box;
    margin-right: 10px;
    border-radius: 50%;
    a {
      display: inline-block;
      width: 32px;
      height: 32px;
      box-sizing: border-box;
      margin: 0;
    }
    img {
      width: 100%;
      height: 100%;
      border-radius: 50%;
    }
  }
`;
const StyledLinkWrapper = styled.div`
  display: flex;
  align-items: center;
  min-height: 32px;
  a {
    font-size: 1em;
    color: #000;
    &:hover {
      opacity: 0.5;
    }
  }
`;
export {
  StyledDiscussionTr,
  StyledData,
  StyledPostInfo,
  StyledAvatarWrapper,
  StyledLinkWrapper,
};
