import { configureStore } from "@reduxjs/toolkit";
import moviesSlice from "@/services/movies/moviesSlice";
import { themoviedbApiRTKQuery } from "@/api/themoviedbApi";
export const store = configureStore({
  reducer: {
    movies: moviesSlice,
    [themoviedbApiRTKQuery.reducerPath]: themoviedbApiRTKQuery.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(themoviedbApiRTKQuery.middleware),
});
