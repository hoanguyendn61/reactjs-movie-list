import styled from "styled-components";
const StyledLeaders = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  p {
    font-size: 1em;
    a {
      color: #000;
      &:hover {
        color: rgba(0, 0, 0, 0.5);
      }
    }
  }
`;
const StyledEditLeader = styled.div`
  width: 100%;
  height: 45px;
  box-sizing: border-box;
  margin-bottom: 20px;
  display: flex;
  align-items: center;
`;
const StyledAvatar = styled.div`
  width: 45px;
  height: 45px;
  a {
    display: inline-block;
    width: 45px;
    height: 45px;
    img {
      width: 100%;
      height: 100%;
      box-sizing: border-box;
      border-radius: 50%;
    }
  }
`;
const StyledInfo = styled.div`
  padding-left: 10px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  p {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0;
    font-weight: 600;
    a {
      color: #000;
      font-weight: 300;
    }
  }
`;
export { StyledLeaders, StyledEditLeader, StyledInfo, StyledAvatar };
