import styled from "styled-components";

export const StyledVisit = styled.div`
  height: 30px;
  margin-bottom: 30px;
`;

export const StyledSocialLink = styled.a`
  position: relative;
  display: block;
  color: #262626;
`;
export const StyledPopup = styled.div`
  position: absolute;
  left: 50%;
  transform: translate(-50%, 0);
  top: -120%;
  background: var(--primary);
  color: #fff;
  white-space: nowrap;
  padding: 8px;
  border-radius: 0.25rem;
  z-index: 10;
  :after {
    content: "";
    position: absolute;
    top: 100%;
    left: 0;
    right: 0;
    margin: 0 auto;
    width: 0;
    height: 0;
    border-top: 5px solid var(--primary);
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
  }
`;
