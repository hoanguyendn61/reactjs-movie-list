import styled from "styled-components";

const StyledWindowTitlebar = styled.div`
  width: 100%;
  background-color: rgb(0, 0, 0);
  border-color: rgb(0, 0, 0);
  color: rgb(255, 255, 255);
  border-top-left-radius: 0.25rem;
  border-top-right-radius: 0.25rem;
  padding: 1rem 1rem;
  border-width: 0 0 1px;
  border-style: solid;
  white-space: nowrap;
  display: flex;
  flex-direction: row;
  flex-shrink: 0;
  align-items: center;
  justify-content: space-between;
`;
const StyledWindowTitle = styled.span`
  margin: 0;
  font-size: 1.25rem;
  line-height: 1.5;
  text-overflow: ellipsis;
  overflow: hidden;
  cursor: default;
  flex: 1;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  white-space: nowrap;
`;
const StyledWindowAction = styled.div`
  margin: -5em 0;
  margin-inline-end: 0;
  line-height: 1;
  display: flex;
  gap: 0.5rem;
  flex-flow: row nowrap;
  flex-shrink: 0;
  align-items: center;
  vertical-align: top;
  &:hover {
    border-radius: 3px;
    background-color: rgba(255, 255, 255, 0.1);
  }
`;
export { StyledWindowTitlebar, StyledWindowTitle, StyledWindowAction };
