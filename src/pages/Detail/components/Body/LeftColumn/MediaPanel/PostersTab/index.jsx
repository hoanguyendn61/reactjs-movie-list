import { StyledViewMore } from "../index.styles";
import { FaArrowCircleRight } from "react-icons/fa";
import { StyledPosterCard } from "./index.styles";
export default ({ posters }) => {
  // const posters = IMAGES.posters;
  return (
    <>
      {posters.map((item, idx) => {
        return (
          <StyledPosterCard key={idx}>
            <img
              loading="lazy"
              src={`https://image.tmdb.org/t/p/w220_and_h330_face${item.file_path}`}
            />
          </StyledPosterCard>
        );
      })}
      <StyledViewMore>
        <div className="link">
          <p>
            <a href="#">
              View More &nbsp;
              <FaArrowCircleRight />
            </a>
          </p>
        </div>
      </StyledViewMore>
    </>
  );
};
