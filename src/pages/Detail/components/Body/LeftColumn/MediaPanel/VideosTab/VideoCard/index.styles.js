import styled from "styled-components";

const StyledVideoCard = styled.div`
  border-radius: var(--imageBorderRadius);
  border: none
  margin-top: 0;
  display: flex;
  align-items: flex-start;
  box-shadow: 0 2px 8px rgb(0 0 0 / 10%);
  background-color: #fff;
  width: 533px;
  height: 300px;
  .wrapper {
    width: 533px;
    height: 300px;
    background-image: url(${(props) => props.bg_image});
    overflow: hidden;
    box-sizing: border-box;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    a.play_trailer {
      width: 100%;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      div.play_background {
        width: 67px;
        height: 67px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        background: rgba(0, 0, 0, 0.7);
        &:hover{
          color: #999;
        }
      }
    }
  }
`;

export { StyledVideoCard };
