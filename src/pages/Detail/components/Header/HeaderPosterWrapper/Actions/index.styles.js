import styled from "styled-components";

export const StyledActions = styled.ul`
  margin-bottom: 20px;
  width: 100%;
  height: 68px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;
export const Chart = styled.li`
  background: transparent;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  height: 68px;
  margin-right: 20px;
  cursor: pointer;
  .consensus {
    width: 68px;
    height: 68px;
    display: inline-block;
    transition: transform 0.2s;
    transform: scale(1);
    &:hover {
      transform: scale(1.1);
    }
    .outer_ring {
      display: inline-block;
      position: relative;
      width: 68px;
      height: 68px;
      border-radius: 50%;
      padding: 4px;
      background-color: #081c22;
      :after {
        position: absolute;
        content: "%";
        top: 35%;
        left: 69%;
        font-size: 8px;
      }
    }
  }
  .text {
    font-weight: 700;
    margin-left: 6px;
    white-space: pre-line;
  }
`;
export const PlayTrailer = styled.li`
  margin-right: 0;
  margin-left: 20px;
  a {
    border: none;
    background: transparent;
    width: auto;
    height: auto;
    font-weight: 600;
    will-change: opacity;
    transition: linear 0.1s;
    &:hover {
      cursor: pointer;
      opacity: 0.7;
    }
  }
`;
