import styled from "styled-components";
import { StyledSectionColumn } from "../index.styles";

const StyledKeywordsSection = styled(StyledSectionColumn)`
  border-bottom: 1px solid #d7d7d7;
  margin-bottom: 30px;
`;
const StyledKeywordsList = styled.ul`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  padding-bottom: 30px;
`;
const StyledKeywordItem = styled.li`
  margin-right: 5px;
  margin-bottom: 10px;
  line-height: 24px;
  font-size: 0.9em;
  box-sizing: border-box;
  white-space: nowrap;
  a {
    background-color: rgba(0, 0, 0, 0.1);
    border: 1px solid #d7d7d7;
    color: #000;
    padding: 4px 10px;
    border-radius: 4px;
  }
`;
export { StyledKeywordsSection, StyledKeywordsList, StyledKeywordItem };
