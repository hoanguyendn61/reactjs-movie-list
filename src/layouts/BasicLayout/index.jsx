import React, { useState, useEffect } from "react";
import Header from "@/components/Header";
import Footer from "@/components/Footer";
import styles from "./index.module.scss";
export default ({ children }) => {
  const [position, setPosition] = useState(window.pageYOffset);
  const [visible, setVisible] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      let moving = window.pageYOffset;

      setVisible(position > moving);
      setPosition(moving);
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });
  const cls = visible ? true : false;
  return (
    <>
      <div className={styles.container}>
        <Header state={cls} />
        <main className={styles.main_wrapper}>
          <section className={styles.inner_content}>{children}</section>
        </main>
      </div>
      <Footer />
    </>
  );
};
