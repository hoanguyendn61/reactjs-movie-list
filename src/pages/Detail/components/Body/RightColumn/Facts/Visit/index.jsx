import { StyledVisit, StyledSocialLink, StyledPopup } from "./index.styles";
import { useState } from "react";
export default ({ children, message }) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <StyledVisit>
      <StyledSocialLink
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
      >
        {isOpen && <StyledPopup>{message}</StyledPopup>}
        {children}
      </StyledSocialLink>
    </StyledVisit>
  );
};
