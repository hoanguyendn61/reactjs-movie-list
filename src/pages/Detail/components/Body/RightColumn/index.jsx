import {
  StyledButton,
  StyledKeyboardShortcuts,
  StyledRightColumn,
  StyledSplitColumn,
  StyledReportIssue,
} from "./index.styles";
import { StyledRoundedButton } from "../index.styles";
import { FaLock, FaKeyboard } from "react-icons/fa";
import { GoReport } from "react-icons/go";
import Facts from "./Facts";
import Keywords from "./Keywords";
import ContentScore from "./ContentScore";
import Leaderboard from "./Leaderboard";
import PopularityTrend from "./PopularityTrend";
export default ({ facts, keywords }) => {
  return (
    <StyledRightColumn>
      <StyledSplitColumn>
        <div>
          <Facts facts={facts} />
          <Keywords keywords={keywords} />
        </div>
        <div>
          <ContentScore />
          <Leaderboard />
          <PopularityTrend />
        </div>
        <div>
          <StyledRoundedButton>
            <FaLock /> LOGIN TO EDIT
          </StyledRoundedButton>
        </div>
        <StyledKeyboardShortcuts>
          <p>
            <a href="#">
              <FaKeyboard /> &nbsp; Keyboard Shortcuts
            </a>
          </p>
        </StyledKeyboardShortcuts>
        <StyledReportIssue>
          <p>
            <GoReport />
            &nbsp; Login to report an issue
          </p>
        </StyledReportIssue>
      </StyledSplitColumn>
    </StyledRightColumn>
  );
};
