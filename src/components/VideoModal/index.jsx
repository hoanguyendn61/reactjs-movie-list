import {
  StyledWindowAction,
  StyledWindowTitle,
  StyledWindowTitlebar,
} from "./index.styles";
import { IoCloseSharp } from "react-icons/io5";
import YouTube from "react-youtube";
export default ({ name, videoKey, onToggleVideo }) => {
  const optsYoutube = {
    height: "621",
    width: "1105",
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };
  return (
    <>
      <StyledWindowTitlebar>
        <StyledWindowTitle>{name}</StyledWindowTitle>
        <StyledWindowAction>
          <IoCloseSharp
            onClick={onToggleVideo}
            style={{
              opacity: 0.5,
            }}
          />
        </StyledWindowAction>
      </StyledWindowTitlebar>
      <YouTube videoId={videoKey} opts={optsYoutube} />
    </>
  );
};
