import styled from "styled-components";

const StyledCollectionWaypoint = styled.div`
  width: 100%;
  border-radius: var(--imageBorderRadius);
  overflow: hidden;
`;
const StyledHeaderCollection = styled.div`
  background-image: linear-gradient(
      to right,
      rgba(var(--tmdbDarkBlue), 1) 0%,
      rgba(var(--tmdbDarkBlue), 0.6) 100%
    ),
    url("https://image.tmdb.org/t/p/w1440_and_h320_multi_faces${(props) =>
      props.backdrop_path}");
  background-size: cover;
  background-position: 50% 50%;
  display: flex;
  align-items: center;
  align-content: center;
  flex-wrap: wrap;
  height: 258px;
  overflow: hidden;
  background-color: #ccc;
  background-repeat: no-repeat;
  padding: 0 20px;
  div {
    color: white;
    width: 100%;
  }
`;

export { StyledCollectionWaypoint, StyledHeaderCollection };
