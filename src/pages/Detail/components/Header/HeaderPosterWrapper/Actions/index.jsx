import { StyledActions, Chart, PlayTrailer } from "./index.styles";
import { CircularProgressbar } from "react-circular-progressbar";
import { FaList, FaHeart, FaBookmark, FaStar, FaPlay } from "react-icons/fa";
import Tooltip from "./Tooltip";
import "react-circular-progressbar/dist/styles.css";
import { useState } from "react";
import Modal from "styled-react-modal";
import VideoModal from "@/components/VideoModal";
const StyledModal = Modal.styled`
  width: 1105px;
  height: 684px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #000;
`;
export default ({ vote_average, video }) => {
  const percentage = Math.round(vote_average * 10);
  const [isOpen, setOpen] = useState(false);
  const onToggleVideo = () => {
    setOpen((prevState) => !prevState);
  };

  const styleObject = {
    path: {
      stroke:
        percentage >= 70
          ? `rgba(33, 208, 122, ${percentage / 100})`
          : `rgba(210,213,49, ${percentage / 100})`,
      strokeLinecap: "round",
    },
    trail: {
      stroke: percentage >= 70 ? "#204529" : "#423d0f",
      strokeLinecap: "round",
    },
    text: {
      fill: "#fff",
      fontSize: "2.2rem",
      fontWeight: "bold",
    },
  };
  return (
    <StyledActions>
      <Chart>
        <div className="consensus">
          <div className="outer_ring">
            <CircularProgressbar
              value={percentage}
              text={`${percentage}`}
              styles={styleObject}
            />
          </div>
        </div>
        <div className="text">
          User <br /> Score
        </div>
      </Chart>
      <Tooltip action="Login to create and edit custom lists">
        <FaList size={12} />
      </Tooltip>
      <Tooltip action="Login to add this movie to your favorite list">
        <FaHeart size={12} />
      </Tooltip>
      <Tooltip action="Login to add this movie to your watchlist">
        <FaBookmark size={12} />
      </Tooltip>
      <Tooltip action="Login to rate this movie">
        <FaStar size={12} />
      </Tooltip>
      {video && (
        <PlayTrailer>
          <a onClick={onToggleVideo}>
            <FaPlay size={12} /> &nbsp;Play Trailer
          </a>
          <StyledModal
            isOpen={isOpen}
            onEscapeKeydown={() => {}}
            onBackgroundClick={() => {}}
          >
            {/* <YouTube videoId={video_key} opts={optsYoutube} /> */}
            <VideoModal
              name={`Play ${video.name}`}
              videoKey={video.key}
              onToggleVideo={onToggleVideo}
            />
          </StyledModal>
        </PlayTrailer>
      )}
    </StyledActions>
  );
};
