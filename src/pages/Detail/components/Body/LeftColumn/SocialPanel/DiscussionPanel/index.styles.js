import styled from "styled-components";

const StyledDiscussionContainer = styled.div`
  table {
    margin: 0;
    width: 100%;
    thead {
      border-color: #fff;
      display: none;
    }
    tbody {
      width: 100%;
      display: inline-block;
    }
  }
`;
const StyledDiscussionTable = styled.table`
  margin: 0;
  width: 100%;
`;
const StyledDiscussionThead = styled.thead`
  border-color: #fff;
  display: none;
`;
const StyledDiscussionTbody = styled.tbody`
  width: 100%;
  display: inline-block;
`;

export {
  StyledDiscussionContainer,
  StyledDiscussionTable,
  StyledDiscussionThead,
  StyledDiscussionTbody,
};
