import axiosClient from "./axiosClient";
import { API_URL, API_KEY } from "@/constants";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
export const themoviedbApi = {
  getMovies: async (page) => {
    const url = `movie/popular?api_key=${API_KEY}&page=${page}`;
    const response = await axiosClient.get(url);
    return response;
  },
  getMovieDetails: async (movieId) => {
    const url = `movie/${movieId}?api_key=${API_KEY}`;
    const response = await axiosClient.get(url);
    return response;
  },
};

export const themoviedbApiRTKQuery = createApi({
  reducerPath: "themoviedbApi_",
  baseQuery: fetchBaseQuery({ baseUrl: API_URL }),
  endpoints: (builder) => ({
    getMovieDetails: builder.query({
      query: ({ id }) => `movie/${id}?api_key=${API_KEY}`,
    }),
    getMovieInfoByType: builder.query({
      query: ({ id, type }) => `movie/${id}/${type}?api_key=${API_KEY}`,
    }),
    getMovieInfoByAppend: builder.query({
      query: ({ id, append }) =>
        `movie/${id}?api_key=${API_KEY}&append_to_response=${append}`,
    }),
  }),
});
export const {
  useGetMovieDetailsQuery,
  useGetMovieInfoByTypeQuery,
  useGetMovieInfoByAppendQuery,
} = themoviedbApiRTKQuery;
