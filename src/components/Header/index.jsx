import styles from "./index.module.scss";
import { FaPlus, FaSearch } from "react-icons/fa";
import { MOVIES, TV_SHOWS, PEOPLE, MORE } from "@/constants";
import Dropdown from "@/components/common/Dropdown";
import { Link } from "react-router-dom";
import themoviedb from "@/assets/images/themoviedb.svg";
export default ({ state }) => {
  const showClass = state ? styles.header__visible : styles.header__hidden;
  return (
    <div className={`${styles.header} ${showClass}`}>
      <div className={styles.header__content}>
        <div className={styles.header__sub_media}>
          <div className={styles.header__right}>
            <Link to="/" className={styles.header__logo}>
              <img src={themoviedb} alt="logo" width="154" height="20" />
            </Link>
            <ul className={styles.header__nav}>
              <Dropdown overlay={MOVIES}>
                <li className={styles.nav__item}>
                  <Link to="/" className={styles.nav__link}>
                    Movies
                  </Link>
                </li>
              </Dropdown>
              <Dropdown overlay={TV_SHOWS}>
                <li className={styles.nav__item}>
                  <a className={styles.nav__link}>TV Shows</a>
                </li>
              </Dropdown>
              <Dropdown overlay={PEOPLE}>
                <li className={styles.nav__item}>
                  <a className={styles.nav__link}>People</a>
                </li>
              </Dropdown>
              <Dropdown overlay={MORE}>
                <li className={styles.nav__item}>
                  <a className={styles.nav__link}>More</a>
                </li>
              </Dropdown>
            </ul>
          </div>
          <div className={styles.header__left}>
            <ul className={styles.header__nav}>
              <li className={styles.nav__item}>
                <a href="#" className={styles.nav__link}>
                  <FaPlus size={16} />
                </a>
              </li>
              <li className={styles.nav__item}>
                <div className={styles.translate}>EN</div>
              </li>
              <li className={styles.nav__item}>
                <a href="#" className={styles.nav__link}>
                  Login
                </a>
              </li>
              <li className={styles.nav__item}>
                <a href="#" className={styles.nav__link}>
                  Join TMDB
                </a>
              </li>
              <li className={styles.nav__item}>
                <a href="#" className={styles.nav__link}>
                  <FaSearch size={16} color="#01B4E4" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
