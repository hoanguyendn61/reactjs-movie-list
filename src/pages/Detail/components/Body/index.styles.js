import styled from "styled-components";

const StyledBodyWrapper = styled.div`
  margin-top: 0;
  margin-bottom: 0;
  background-color: #fff;
  width: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-items: flex-start;
  align-content: flex-start;
`;
const StyledColumnWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  align-content: flex-start;
`;
const StyledContentWrapper = styled.div`
  max-width: var(--maxPrimaryPageWidth);
  width: 100%;
  display: flex;
  align-items: flex-start;
  align-content: flex-start;
  padding-left: 40px;
  padding-right: 40px;
  padding-top: 30px;
  padding-bottom: 30px;
`;
const StyledRoundedButton = styled.button`
  margin-top: 20px;
  border-radius: 20px;
  background: #000;
  color: #fff;
  padding: 7px 20px;
  font-size: 1em;
  font-weight: bold;
  transition: all 0.2s ease-in-out;
  &:hover {
    background: #fff;
    color: #000;
  }
`;
export {
  StyledBodyWrapper,
  StyledColumnWrapper,
  StyledContentWrapper,
  StyledRoundedButton,
};
