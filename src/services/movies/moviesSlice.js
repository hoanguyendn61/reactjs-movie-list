import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { themoviedbApi } from "@/api/themoviedbApi";
const initialState = {
  movies: [],
  status: "idle",
  loading: false,
  error: null,
};

export const fetchMovies = createAsyncThunk(
  "movies/fetchMovies",
  async (page) => {
    try {
      const response = await themoviedbApi.getMovies(page);
      return {
        page,
        response,
      };
    } catch (error) {
      throw error;
    }
  }
);

const moviesSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchMovies.pending, (state, action) => {
      state.status = "loading";
      state.loading = true;
    }),
      builder.addCase(fetchMovies.fulfilled, (state, action) => {
        if (action.payload.page === 1) {
          state.movies = action.payload.response.results;
        } else {
          state.movies.push(...action.payload.response.results);
        }
        state.status = "succeeded";
        state.loading = false;
      }),
      builder.addCase(fetchMovies.rejected, (state, action) => {
        state.error = action.error.message;
        state.status = "failed";
        state.loading = false;
      });
  },
});
export const selectMovies = (state) => state.movies;
export default moviesSlice.reducer;
