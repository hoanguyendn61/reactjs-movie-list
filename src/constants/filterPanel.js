export const SORT_DATA = [
  {
    id: 1,
    name: "Popularity Descending",
    value: "popularity.desc",
  },
  {
    id: 2,
    name: "Popularity Ascending",
    value: "popularity.asc",
  },
  {
    id: 7,
    name: "Rating Descending",
    value: "rating.desc",
  },
  {
    id: 8,
    name: "Rating Ascending",
    value: "rating.asc",
  },
  {
    id: 3,
    name: "Release Date Descending",
    value: "release_date.desc",
  },
  {
    id: 4,
    name: "Release Date Ascending",
    value: "release_date.asc",
  },
  {
    id: 5,
    name: "Title (A-Z)",
    value: "original_title.asc",
  },
  {
    id: 6,
    name: "Title (Z-A)",
    value: "original_title.desc",
  },
];
