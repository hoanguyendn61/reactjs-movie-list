import styled from "styled-components";

const StyledContentWrapper = styled.div`
  width: 533px;
  height: 300px;
`;
const StyledViewMore = styled.div`
  width: 200px;
  height: 300px;
  margin-left: 20px;
  div.link {
    width: 200px;
    height: 300px;
    display: inline-flex;
    align-items: center;
    box-sizing: border-box;
    p {
      padding-left: 4px;
      margin: 0;
      a {
        display: inline-flex;
        align-items: center;
        font-size: 1.3rem;
        color: #000;
        &:hover {
          opacity: 0.5;
        }
      }
    }
  }
`;
export { StyledContentWrapper, StyledViewMore };
