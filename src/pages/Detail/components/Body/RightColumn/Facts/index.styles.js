import styled from "styled-components";

const StyledSocialLinks = styled.div`
  display: flex;
  div {
    height: 30px;
    margin-bottom: 30px;
    a {
      color: #262626;
    }
  }
  div + div {
    margin-left: 20px;
    &.homepage {
      border-left: 1px solid #d7d7d7;
      padding-left: 8px;
    }
  }
`;
const StyledFact = styled.p`
  box-sizing: border-box;
  margin: 0 0 20px 0;
  font-size: 1em;
  strong {
    display: block;
    font-weight: 600;
  }
`;
const ActionPopup = styled.div`
  background: var(--primary);
  padding: 8px;
  border-radius: 0.25rem;
`;
export { StyledSocialLinks, StyledFact, ActionPopup };
