import styles from "./index.module.scss";
import MovieCard from "@/components/MovieCard";
import { useState, useRef, useCallback } from "react";
import useMovie from "@/hooks/useMovie";
import LoadingBar from "react-top-loading-bar";
export default ({}) => {
  const [pageNumber, setPageNumber] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const { loading, movies } = useMovie(pageNumber);
  const barRef = useRef(null);
  const moviesObserver = useRef();
  const lastMovieElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (moviesObserver.current) moviesObserver.current.disconnect();
      moviesObserver.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && loadMore) {
          barRef.current.continuousStart();
          setTimeout(() => {
            setPageNumber(pageNumber + 1);
            barRef.current.complete();
          }, 600);
        }
      });
      if (node) moviesObserver.current.observe(node);
    },
    [loading]
  );
  const onHandleLoadMore = () => {
    setLoadMore(true);
    barRef.current.continuousStart();
    setTimeout(() => {
      setPageNumber((prevPage) => prevPage + 1);
      barRef.current.complete();
    }, 600);
  };
  return (
    <div className={styles.panel_results}>
      <LoadingBar color="#01b4e4" ref={barRef} />
      <div className={styles.page_wrapper}>
        {movies.map((movie, index) => {
          if (movies.length === index + 1) {
            return (
              <MovieCard ref={lastMovieElementRef} key={index} {...movie} />
            );
          } else {
            return <MovieCard key={index} {...movie} />;
          }
        })}
        <div className={styles.infinite_btn} onClick={onHandleLoadMore}>
          <p>Load More</p>
        </div>
      </div>
    </div>
  );
};
