import {
  StyledDiscussionTable,
  StyledDiscussionThead,
  StyledDiscussionTbody,
} from "./index.styles";
import { StyledNewButton } from "../../../LeftColumn/index.styles";
import { StyledDiscussionTr } from "./DiscussionCard/index.styles";
import DiscussionCard from "./DiscussionCard";
import DISCUSSIONS from "@/constants/discussions";
export default () => {
  return (
    <div>
      <StyledDiscussionTable>
        <StyledDiscussionThead>
          <StyledDiscussionTr>
            <th>Subject</th>
            <th>Status</th>
            <th>Replies</th>
            <th>Last Reply</th>
          </StyledDiscussionTr>
        </StyledDiscussionThead>
        <StyledDiscussionTbody>
          {DISCUSSIONS.map((item, index) => {
            return <DiscussionCard key={index} {...item} />;
          })}
        </StyledDiscussionTbody>
      </StyledDiscussionTable>
      <StyledNewButton>
        <a href="#">Go to Discussions</a>
      </StyledNewButton>
    </div>
  );
};
