import styled from "styled-components";

const StyledDropdown = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

const StyledOpen = styled.ul`
  position: absolute;
  top: ${(props) => (props.submenu ? "-31%" : "100%")};
  left: ${(props) => (props.submenu ? "100%" : "0")};
  background: var(--white);
  z-index: 111;
  padding: 8px 0;
  border: 1px solid #d9d9d9;
  border-radius: 0.25rem;
`;

const StyledItem = styled.li`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  color: black;
  box-sizing: border-box;
  cursor: pointer;
  font-size: 0.9rem;
  padding: 5px 1.5rem;
  z-index: 1;
  flex-wrap: nowrap;
  white-space: nowrap;
  &:hover {
    background-color: #f8f9fa;
  }
`;
const StyledCount = styled.div`
  color: rgba(0, 0, 0, 0.4) !important;
  font-weight: 300;
  margin-left: 1.6rem;
`;
export { StyledDropdown, StyledOpen, StyledItem, StyledCount };
