import styled from "styled-components";

export const StyledTitle = styled.div`
  width: 100%;
  margin-bottom: 24px;
  display: flex;
  flex-wrap: wrap;
`;
export const OriginalTitle = styled.h2`
  width: 100%;
  margin: 0;
  padding: 0;
  font-size: 2.2rem;
  a {
    color: #fff;
    font-weight: 700;
    &: hover {
      color: #b8b3b4;
    }
  }
  .release_date {
    opacity: 0.8;
    font-weight: 400;
  }
`;
export const Facts = styled.div`
  display: flex;
  span.certification {
    border: 1px solid rgba(255, 255, 255, 0.6);
    color: rgba(255, 255, 255, 0.6);
    display: inline-flex;
    white-space: nowrap;
    align-items: center;
    align-content: center;
    padding: 0.06em 4px 0.15em 4px !important;
    line-height: 1;
    border-radius: 2px;
    margin-right: 7px;
  }
  span.certification + span {
    padding-left: 0;
    ::before {
      content: "";
    }
  }
  span + span {
    padding-left: 20px;
    position: relative;
    top: 0;
    left: 0;
    a {
      &: hover {
        color: #b8b3b4;
      }
    }
  }
  span + span:before {
    content: "●";
    font-size: 0.6em;
    line-height: 1;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 7px;
    display: inline-flex;
    align-content: center;
    align-items: center;
    z-index: -1;
  }
`;
