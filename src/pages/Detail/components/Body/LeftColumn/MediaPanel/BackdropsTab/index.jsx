import { StyledViewMore } from "../index.styles";
import { FaArrowCircleRight } from "react-icons/fa";
import { StyledBackdropCard } from "./index.styles";
export default ({ backdrops }) => {
  return (
    <>
      {backdrops.map((item, idx) => {
        return (
          <StyledBackdropCard key={idx}>
            <img
              loading="lazy"
              src={`https://image.tmdb.org/t/p/w533_and_h300_bestv2${item.file_path}`}
            />
          </StyledBackdropCard>
        );
      })}
      <StyledViewMore>
        <div className="link">
          <p>
            <a href="#">
              View More &nbsp;
              <FaArrowCircleRight />
            </a>
          </p>
        </div>
      </StyledViewMore>
    </>
  );
};
