import { StyledHeaderPosterWrapper, HeaderSection } from "./index.styles";
import Title from "./Title";
import Actions from "./Actions";
import HeaderInfo from "./HeaderInfo";
import Credits from "./Credits";
export default ({ results }) => {
  const getVideoOrTeaser = (listVideos) => {
    const video = listVideos.find((video) => video.type === "Trailer");
    if (video) {
      return video;
    } else {
      return listVideos[0];
    }
  };
  const video = getVideoOrTeaser(results?.videos?.results);
  return (
    <StyledHeaderPosterWrapper>
      <HeaderSection>
        <Title {...results} />
        <Actions vote_average={results.vote_average} video={video} />
        <HeaderInfo tagline={results.tagline} overview={results.overview} />
        <Credits crew={results.credits.crew} />
      </HeaderSection>
    </StyledHeaderPosterWrapper>
  );
};
