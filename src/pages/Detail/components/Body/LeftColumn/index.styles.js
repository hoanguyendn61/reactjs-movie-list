import styled from "styled-components";
const StyledLeftColumn = styled.div`
  background-color: #fff;
  width: calc(100vw - 80px - 268px);
  max-width: calc(var(--maxPrimaryPageWidth) - 80px - 268px);
  display: flex;
  flex-wrap: wrap;
  flex: 0 1 auto;
  padding-right: 30px;
`;
const StyledSectionPanel = styled.section`
  width: 100%;
  display: block;
  padding: 30px 0;
  border-top: 1px solid #d7d7d7;
`;
const StyledAuto = styled.h3`
  font-weight: 600;
  font-size: 1.4em;
  margin-bottom: 20px;
`;
const StyledMenuAuto = styled(StyledAuto)`
  display: inline-block;
  margin-right: 50px;
`;
const StyledNewButton = styled.p`
  margin: 20px 0 0 0;
  display: inline-block;
  a {
    color: #000;
    font-weight: 600;
    font-size: 1em;
    &:hover {
      color: rgba(0, 0, 0, 0.5);
    }
  }
`;
const StyledMenu = styled.div`
  display: flex;
  box-sizing: border-box;
  width: 100%;
  align-items: baseline;
  ul {
    width: 100%;
    box-sizing: border-box;
    li {
      display: inline-block;
      margin-right: 24px;
      padding-bottom: 5px;
      font-size: 1.1em;
      box-sizing: border-box;
      &.active {
        border-bottom: 4px solid #000;
      }
      a {
        color: #000;
        font-weight: 600;
        cursor: pointer;
        span.count {
          opacity: 0.6;
        }
      }
    }
  }
`;
const StyledScrollerWrapper = styled.div`
  position: relative;
  top: 0;
  left: 0;
`;
const StyledScroller = styled.div`
  overflow-y: hidden;
  overflow-x: scroll;
  white-space: nowrap;
  padding-bottom: ${(props) => (props.media ? "0" : "10px")};
  display: flex;
  width: 100%;
  border-radius: var(--imageBorderRadius);
`;
const StyledBlur = styled.div(({ hideFade }) => ({
  width: "60px",
  height: " 100%",
  position: "absolute",
  top: 0,
  right: 0,
  backgroundImage:
    "linear-gradient(to right, rgba(255,255,255,0) 0%, #fff 100%)",
  pointerEvents: "none",
  transition: "opacity 0.5s ease-out",
  opacity: hideFade ? 0 : 1,
}));
export {
  StyledLeftColumn,
  StyledAuto,
  StyledMenuAuto,
  StyledNewButton,
  StyledMenu,
  StyledSectionPanel,
  StyledScrollerWrapper,
  StyledScroller,
  StyledBlur,
};
