import {
  StyledSectionPanel,
  StyledMenu,
  StyledMenuAuto,
} from "../index.styles";
import { StyledContent } from "./index.styles";
import ReviewPanel from "./ReviewPanel";
import DiscussionPanel from "./DiscussionPanel";
import { useState } from "react";
export default ({ reviews }) => {
  const [isReviewsClicked, setIsReviewsClicked] = useState(true);
  const [reviewsCount, setReviewsCount] = useState(0);
  const [discussionCount, setDiscussionCount] = useState(0);
  const onSetReviewsCount = (count) => {
    setReviewsCount(count);
  };
  return (
    <StyledSectionPanel>
      <StyledMenu>
        <StyledMenuAuto>Social</StyledMenuAuto>
        <ul>
          <li className={isReviewsClicked ? "active" : ""}>
            <a role="button" onClick={() => setIsReviewsClicked(true)}>
              Reviews <span className="count">{reviewsCount}</span>
            </a>
          </li>
          <li className={isReviewsClicked ? "" : "active"}>
            <a onClick={() => setIsReviewsClicked(false)} role="button">
              Discussion <span className="count">5</span>
            </a>
          </li>
        </ul>
      </StyledMenu>
      <StyledContent>
        <div className="original_content">
          {isReviewsClicked ? (
            <ReviewPanel
              onSetReviewsCount={onSetReviewsCount}
              reviews={reviews}
            />
          ) : (
            <DiscussionPanel />
          )}
        </div>
      </StyledContent>
    </StyledSectionPanel>
  );
};
