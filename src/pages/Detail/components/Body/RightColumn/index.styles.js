import styled from "styled-components";

const StyledRightColumn = styled.div`
  min-width: 260px;
  width: 260px;
  display: flex;
  flex-wrap: wrap;
`;
const StyledSplitColumn = styled.section`
  padding-top: 4px;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  max-width: 300px;
  > div {
    width: 100%;
  }
`;
const StyledSectionColumn = styled.section`
  margin-bottom: 30px;
  width: 100%;
  h4 {
    font-size: 1.1em;
    font-weight: 600;
    margin-bottom: 10px;
  }
`;
const StyledButton = styled.button`
  border-radius: 20px;
  background: #000;
  color: #fff;
  padding: 6px 20px;
  font-size: 0.9em;
  font-weight: bold;
`;
const StyledKeyboardShortcuts = styled.div`
  margin-top: 30px;
  p {
    a {
      display: inline-flex;
      align-items: center;
      align-content: center;
      margin: 0;
      opacity: 0.5;
      color: #000;
      &:hover {
        opacity: 0.25;
      }
    }
  }
`;
const StyledReportIssue = styled.div`
  margin-top: 20px;
  display: flex;
  align-items: center;
  align-content: center;
  p {
    margin: 0;
    display: flex;
    align-items: center;
    align-content: center;
  }
`;
export {
  StyledRightColumn,
  StyledSplitColumn,
  StyledSectionColumn,
  StyledButton,
  StyledKeyboardShortcuts,
  StyledReportIssue,
};
