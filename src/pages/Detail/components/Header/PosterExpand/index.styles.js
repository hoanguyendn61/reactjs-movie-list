import styled from "styled-components";

const StyledInfoPoster = styled.div`
  position: relative;
  top: 0;
  left: 0;
  width: 360px;
  box-sizing: border-box;
  background: #fff;
  a.close_right {
    padding: 20px 20px 0 0;
    float: right;
  }
  a {
    color: #000;
    display: inline-flex;
    align-items: center;
  }
`;
const StyledPosterExpand = styled.section`
  div#lightbox_popup {
    display: flex;
  }
  a.wrapper {
    width: 390px;
    height: 585px;
    // background-image: url("https://image.tmdb.org/t/p/w600_and_h900_bestv2/pIkRyD18kl4FhoCNQuWxWu5cBLM.jpg");
    // background-repeat: no-repeat;
    // background-position: center;
  }
`;
const StyledMetaWrapper = styled.div`
  width: 100%;
  position: absolute;
  bottom: 20px;
  box-sizing: border-box;
  h3 {
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid #e3e3e3;
    padding-bottom: 14px !important;
    margin-bottom: 7px;
    font-size: 1em;
    font-weight: 400;
    white-space: normal;
    padding: 0 20px;
  }
  div#paging {
    margin: 20px 20px 0 20px;
    a.next {
      font-size: 1.6em;
      float: right;
    }
  }
`;
const StyledForm = styled.form`
  padding: 0 20px;
  label {
    color: #333;
    margin: 16px 0 0 0;
    display: block;
    width: 100%;
    font-weight: normal;
    font-size: 0.8em;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    align-content: center;
    margin-bottom: 0.375rem;
  }
  label.flex {
    display: inline-flex;
    align-items: center;
  }
  span.flex {
    margin-bottom: 0.375rem;
  }
  label:first-child {
    margin-top: 0;
  }
  p {
    a {
      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
  }
`;
const StyledTaggedPeopleWrapper = styled.div`
  margin-top: 40px;
  margin-bottom: 40px;
  ul.tagged_people {
    display: inline-block;
    margin: 0 0 0 20px;
    padding: 0;
    list-style-type: none;
  }
`;
export {
  StyledInfoPoster,
  StyledPosterExpand,
  StyledMetaWrapper,
  StyledForm,
  StyledTaggedPeopleWrapper,
};
