import {
  StyledReviewCard,
  StyledReviewContainer,
  StyledGrouped,
  StyledInfo,
  StyledTeaser,
} from "./index.styles";
import { StyledNewButton } from "../../index.styles";
import { useEffect, useState } from "react";
import dateFormat from "dateformat";
import MarkdownIt from "markdown-it";
export default ({ onSetReviewsCount, reviews }) => {
  const [review, setReview] = useState(null);
  const md = new MarkdownIt();
  const getRandomReview = () => {
    let randomReview = {};
    const randomIndex = Math.floor(Math.random() * reviews.total_results);
    randomReview = reviews.results[randomIndex];
    return randomReview;
  };
  useEffect(() => {
    if (reviews) {
      setReview(getRandomReview());
      onSetReviewsCount(reviews.total_results);
    }
  }, [reviews]);
  return (
    <StyledReviewContainer>
      <div className="content">
        {review ? (
          <div className="inner_content">
            <StyledReviewCard>
              <StyledGrouped>
                <div className="avatar">
                  <a href="#">
                    <img
                      src={`https://www.gravatar.com/avatar${review?.author_details.avatar_path}`}
                      alt={review?.author}
                      className="avatar"
                    />
                  </a>
                </div>
                <StyledInfo>
                  <h4>Featured Review</h4>
                  <div className="rating_wrapper"></div>
                  <h5>
                    Written by <a href="#">{review?.author_details.name}</a> on{" "}
                    {dateFormat(review?.created_at, "mmmm d, yyyy")}
                  </h5>
                </StyledInfo>
              </StyledGrouped>
              {review && (
                <StyledTeaser>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: md.render(review.content),
                    }}
                  ></div>
                </StyledTeaser>
              )}
            </StyledReviewCard>
            <StyledNewButton>
              <a href="#">Read All Reviews</a>
            </StyledNewButton>
          </div>
        ) : (
          <p>We don't have any reviews</p>
        )}
      </div>
    </StyledReviewContainer>
  );
};
