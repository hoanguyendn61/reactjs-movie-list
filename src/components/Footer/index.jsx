import styles from "./index.module.scss";
import themoviedb_footer from "@/assets/images/themoviedb_footer.svg";
export default () => {
  return (
    <div className={styles.footer}>
      <nav>
        <div className={styles.join}>
          <img
            src={themoviedb_footer}
            alt="The Movie Database (TMDB)"
            width="130"
            height="94"
          />
          <a className={styles.rounded} href="#">
            Join the Community
          </a>
        </div>
        <div>
          <h3>The Basics</h3>
          <ul>
            <li>
              <a href="/about">About TMDB</a>
            </li>
            <li>
              <a href="/about/staying-in-touch">Contact Us</a>
            </li>
            <li>
              <a href="/talk">Support Forums</a>
            </li>
            <li>
              <a href="/documentation/api">API</a>
            </li>
            <li>
              <a
                href="https://status.themoviedb.org/"
                target="_blank"
                rel="noopener"
              >
                System Status
              </a>
            </li>
          </ul>
        </div>
        <div>
          <h3>Get Involved</h3>
          <ul>
            <li>
              <a href="/bible">Contribution Bible</a>
            </li>
            <li>
              <a href="/movie/new">Add New Movie</a>
            </li>
            <li>
              <a href="/tv/new">Add New TV Show</a>
            </li>
          </ul>
        </div>
        <div>
          <h3>Community</h3>
          <ul>
            <li>
              <a href="/documentation/community/guidelines">Guidelines</a>
            </li>
            <li>
              <a href="/discuss">Discussions</a>
            </li>
            <li>
              <a href="/leaderboard">Leaderboard</a>
            </li>
            <li>
              <a
                href="https://twitter.com/themoviedb"
                target="_blank"
                rel="noopener"
              >
                Twitter
              </a>
            </li>
          </ul>
        </div>
        <div>
          <h3>Legal</h3>
          <ul>
            <li>
              <a href="/documentation/website/terms-of-use">Terms of Use</a>
            </li>
            <li>
              <a href="/documentation/api/terms-of-use">API Terms of Use</a>
            </li>
            <li>
              <a href="/privacy-policy">Privacy Policy</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};
