import {
  StyledDiscussionTr,
  StyledData,
  StyledPostInfo,
  StyledAvatarWrapper,
  StyledLinkWrapper,
} from "./index.styles";
export default ({
  avatar_path,
  topic,
  replies,
  status,
  last_reply,
  date_posted,
}) => {
  return (
    <StyledDiscussionTr>
      <StyledData>
        <StyledPostInfo>
          <div className="flex_wrapper">
            <StyledAvatarWrapper>
              <span className="avatar">
                <a href="#">
                  <img
                    src={`https://image.tmdb.org/t/p/w45_and_h45_face${avatar_path}`}
                    alt={topic}
                    loading="lazy"
                  />
                </a>
              </span>
            </StyledAvatarWrapper>
            <StyledLinkWrapper>
              <a href="#" className="topic">
                {topic}
              </a>
            </StyledLinkWrapper>
          </div>
        </StyledPostInfo>
      </StyledData>
      <StyledData>
        <p className="status">{status}</p>
      </StyledData>
      <StyledData>
        <p className="replies">{replies}</p>
      </StyledData>
      <StyledData>
        <p>
          {date_posted} <br />{" "}
          <span className="username">
            by <a href="#">{last_reply}</a>
          </span>
        </p>
      </StyledData>
    </StyledDiscussionTr>
  );
};
