import styles from "./index.module.scss";
import dateFormat from "dateformat";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { forwardRef } from "react";
import { AiOutlinePercentage } from "react-icons/ai";
import { Link } from "react-router-dom";
const MovieCard = (
  { id, original_title, title, release_date, poster_path, vote_average },
  ref
) => {
  const percentage = vote_average * 10;
  const styleObject = {
    path: {
      stroke:
        percentage >= 70
          ? `rgba(33, 208, 122, ${percentage / 100})`
          : `rgba(210,213,49, ${percentage / 100})`,
      strokeLinecap: "round",
    },
    trail: {
      stroke: percentage >= 70 ? "#204529" : "#423d0f",
      strokeLinecap: "round",
    },
    text: {
      fill: "#fff",
      fontSize: "2.3rem",
      fontWeight: "bold",
    },
  };
  return (
    <div className={styles.card} ref={ref}>
      <div className={styles.card__image}>
        <div className={styles.wrapper}>
          <Link to={`/movie/${id}`} className={styles.image}>
            <img
              loading="lazy"
              src={`https://image.tmdb.org/t/p/w220_and_h330_face${poster_path}`}
              alt={original_title}
            />
          </Link>
        </div>
      </div>
      <div className={styles.card__content}>
        <div className={styles.consensus}>
          <div className={styles.outer_ring}>
            <div className={styles.progress}>
              <CircularProgressbar
                value={percentage}
                text={`${percentage}`}
                styles={styleObject}
              />
              <div className={styles.symbol}>
                <AiOutlinePercentage size={7} />
              </div>
            </div>
          </div>
        </div>
        <h2>
          <Link to={`/movie/${id}`}>{title}</Link>
        </h2>
        <p>{dateFormat(release_date, "mediumDate")}</p>
      </div>
    </div>
  );
};
export default forwardRef(MovieCard);
