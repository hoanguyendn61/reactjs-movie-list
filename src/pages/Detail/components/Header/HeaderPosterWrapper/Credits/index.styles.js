import styled from "styled-components";

const StyledCredits = styled.ol`
  width: 100%;
  margin-top: 20px;
  justify-content: flex-start;
  flex-wrap: wrap;
  list-style-type: none;
  list-style-position: inside;
  padding: 0;
  display: flex;
  position: relative;
  top: 0;
  left: 0;
`;
const StyledProfile = styled.li`
  background-color: transparent;
  height: auto;
  margin-bottom: 18px;
  flex-basis: 33%;
  text-align: left;
  margin-right: 0;
  box-sizing: border-box;
  padding-right: 20px;
  min-width: 200px;
  p {
    font-size: 1em;
    margin: 0;
    padding: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    a {
      font-weight: bold;
      &:hover {
        color: #b8b3b4;
      }
    }
  }
  p.character {
    font-size: 0.8em;
  }
`;
export { StyledCredits, StyledProfile };
