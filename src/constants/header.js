export const MOVIES = [
  {
    key: "1",
    label: "Popular",
  },
  {
    key: "2",
    label: "Now Playing",
  },
  {
    key: "3",
    label: "Upcoming",
  },
  {
    key: "4",
    label: "Top Rated",
  },
];
export const TV_SHOWS = [
  {
    key: "1",
    label: "Popular",
  },
  {
    key: "2",
    label: "Airing Today",
  },
  {
    key: "3",
    label: "On TV",
  },
  {
    key: "4",
    label: "Top Rated",
  },
];
export const PEOPLE = [
  {
    key: "1",
    label: "Popular People",
  },
];
export const MORE = [
  {
    key: "1",
    label: "Discussions",
  },
  {
    key: "2",
    label: "Leaderboard",
  },
  {
    key: "3",
    label: "Support",
  },
  {
    key: "4",
    label: "API",
  },
];
