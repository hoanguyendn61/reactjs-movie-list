import { StyledHeaderInfo, Tagline } from "./index.styles";
export default ({ tagline, overview }) => {
  return (
    <StyledHeaderInfo>
      <Tagline>{tagline}</Tagline>
      <h3
        style={{
          width: "100%",
          marginTop: "10px",
          marginBottom: "8px",
        }}
      >
        Overview
      </h3>
      <div className="overview">
        <p>{overview}</p>
      </div>
    </StyledHeaderInfo>
  );
};
