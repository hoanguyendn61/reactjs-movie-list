import { StyledActorCard } from "./index.styles";
import default_profile from "@/assets/images/default_profile.svg";
export default ({ profile_path, name, character }) => {
  return (
    <StyledActorCard>
      <a href="#">
        {profile_path ? (
          <img
            loading="lazy"
            src={`https://image.tmdb.org/t/p/w138_and_h175_face${profile_path}`}
            alt={name}
          />
        ) : (
          <img
            src={default_profile}
            width={138}
            height={175}
            style={{ background: "#e5e5e5" }}
          />
        )}
      </a>
      <p>
        <a href="#">{name}</a>
      </p>
      <p className="character">{character}</p>
    </StyledActorCard>
  );
};
