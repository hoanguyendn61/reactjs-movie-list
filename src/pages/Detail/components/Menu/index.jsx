import {
  StyledShortcutBarScroller,
  StyledMenuBar,
  StyledMenuItem,
} from "./index.styles";
import { GoTriangleDown } from "react-icons/go";
import { MENU } from "@/constants";
import Dropdown from "@/components/common/Dropdown";
export default () => {
  return (
    <StyledShortcutBarScroller>
      <StyledMenuBar>
        <StyledMenuItem active={true}>
          <Dropdown overlay={MENU[0].children}>
            <span>
              {MENU[0].label} &nbsp;
              <GoTriangleDown size={13} />
            </span>
          </Dropdown>
        </StyledMenuItem>
        <StyledMenuItem>
          <Dropdown overlay={MENU[1].children}>
            <span>
              {MENU[1].label} &nbsp;
              <GoTriangleDown size={13} />
            </span>
          </Dropdown>
        </StyledMenuItem>
        <StyledMenuItem>
          <Dropdown overlay={MENU[2].children}>
            <span>
              {MENU[2].label} &nbsp;
              <GoTriangleDown size={13} />
            </span>
          </Dropdown>
        </StyledMenuItem>
        <StyledMenuItem>
          <Dropdown overlay={MENU[3].children}>
            <span>
              {MENU[3].label} &nbsp;
              <GoTriangleDown size={13} />
            </span>
          </Dropdown>
        </StyledMenuItem>
      </StyledMenuBar>
    </StyledShortcutBarScroller>
  );
};
