import styled from "styled-components";

export const StyledTooltip = styled.li`
  padding: 3px 0;
  margin-left: 10px;
  margin-right: 10px;
  .box_tooltip {
    position: relative;
    box-sizing: border-box;
    background: rgba(var(--tmdbDarkBlue), 1);
    border-radius: 50%;
    width: 46px;
    height: 46px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    align-content: center;
  }
`;
export const ActionPopup = styled.div`
  position: absolute;
  bottom: -94%;
  background: var(--primary);
  white-space: nowrap;
  padding: 8px;
  border-radius: 0.25rem;
  z-index: 10;
  :before {
    position: absolute;
    top: -6px;
    left: 51%;
    margin-left: -10px;
    content: "";
    display: block;
    border-left: 8px solid transparent;
    border-right: 8px solid transparent;
    border-bottom: 8px solid rgba(var(--tmdbDarkBlue), 1);
  }
`;
