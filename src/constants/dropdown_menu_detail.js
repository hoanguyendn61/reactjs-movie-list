export const MENU = [
  {
    label: "Overview",
    children: [
      {
        label: "Main",
      },
      {
        label: "Alternative Titles",
      },
      {
        label: "Cast & Crew",
      },
      {
        label: "Release Dates",
      },
      {
        label: "Translations",
      },
      {
        label: "Watch Now",
      },
      {
        label: "Changes",
      },
    ],
  },
  {
    label: "Media",
    children: [
      {
        label: "Backdrops",
        count: 18,
      },
      {
        label: "Logo",
        count: 19,
      },
      {
        label: "Poster",
        count: 98,
      },
      {
        label: "Videos",
        children: [
          {
            label: "Trailers",
            count: 8,
          },
          {
            label: "Teasers",
            count: 10,
          },
          {
            label: "Clips",
            count: 13,
          },
          {
            label: "Behind the Scences",
            count: 9,
          },
          {
            label: "Featurettes",
            count: 6,
          },
        ],
      },
    ],
  },
  {
    label: "Fandom",
    children: [
      {
        label: "Discussions",
        children: [
          {
            label: "Overview",
          },
          {
            label: "General",
            count: 4,
          },
          {
            label: "Content Issues",
            count: 3,
          },
        ],
      },
      {
        label: "Reviews",
      },
    ],
  },
  {
    label: "Share",
    children: [
      {
        label: "Share Link",
      },
      {
        label: "Facebook",
      },
      {
        label: "Tweet",
      },
    ],
  },
];
