import {
  StyledSectionPanel,
  StyledMenu,
  StyledMenuAuto,
  StyledScrollerWrapper,
  StyledScroller,
  StyledBlur,
} from "../index.styles";
import { useState } from "react";
import VideosTab from "./VideosTab";
import BackdropsTab from "./BackdropsTab";
import PostersTab from "./PostersTab";
import MostPopularTab from "./MostPopularTab";
export default ({ videos, posters, backdrops }) => {
  const [selectedTab, setSelectedTab] = useState(0);
  const onClickTab = (tabId) => {
    setSelectedTab(tabId);
  };
  const [scrollPosition, setScrollPosition] = useState(0);
  const onScroll = (e) => {
    const position = e.target.scrollLeft;
    setScrollPosition(position);
  };
  const onRenderTab = (tabId) => {
    switch (tabId) {
      case 0:
        if (videos && backdrops && posters) {
          return (
            <MostPopularTab
              video={videos[0]}
              backdrop={backdrops[0]}
              poster={posters[0]}
            />
          );
        }
        break;
      case 1:
        return <VideosTab videos={videos.slice(0, 6)} />;
      case 2:
        return <BackdropsTab backdrops={backdrops.slice(0, 6)} />;
      case 3:
        return <PostersTab posters={posters.slice(0, 6)} />;
      default:
        break;
    }
  };
  return (
    <StyledSectionPanel>
      <StyledMenu>
        <StyledMenuAuto>Media</StyledMenuAuto>
        <ul>
          <li className={selectedTab === 0 ? "active" : ""}>
            <a role="button" onClick={() => onClickTab(0)}>
              Most Popular
            </a>
          </li>
          <li className={selectedTab === 1 ? "active" : ""}>
            <a role="button" onClick={() => onClickTab(1)}>
              Videos <span className="count">{videos && videos.length}</span>
            </a>
          </li>
          <li className={selectedTab === 2 ? "active" : ""}>
            <a role="button" onClick={() => onClickTab(2)}>
              Backdrops{" "}
              <span className="count">{backdrops && backdrops.length}</span>
            </a>
          </li>
          <li className={selectedTab === 3 ? "active" : ""}>
            <a role="button" onClick={() => onClickTab(3)}>
              Posters <span className="count">{posters && posters.length}</span>
            </a>
          </li>
        </ul>
      </StyledMenu>
      <StyledScrollerWrapper>
        <StyledScroller media="1" onScroll={onScroll}>
          {onRenderTab(selectedTab)}
        </StyledScroller>
        <StyledBlur hideFade={scrollPosition > 100 ? "1" : null} />
      </StyledScrollerWrapper>
    </StyledSectionPanel>
  );
};
