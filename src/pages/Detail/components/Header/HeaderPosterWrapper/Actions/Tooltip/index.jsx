import { StyledTooltip, ActionPopup } from "./index.styles";
import React from "react";
export default ({ children, action }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  return (
    <StyledTooltip
      onMouseEnter={() => setIsOpen(true)}
      onMouseLeave={() => setIsOpen(false)}
    >
      <div className="box_tooltip">
        {children}
        {isOpen && <ActionPopup>{action}</ActionPopup>}
      </div>
    </StyledTooltip>
  );
};
