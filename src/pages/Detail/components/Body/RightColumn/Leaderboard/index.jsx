import { StyledSectionColumn } from "../index.styles";
import {
  StyledLeaders,
  StyledEditLeader,
  StyledInfo,
  StyledAvatar,
} from "./index.styles";
import { TOP_CONTRIBUTORS } from "@/constants";
export default () => {
  return (
    <StyledSectionColumn>
      <h4>Top Contributors</h4>
      <StyledLeaders>
        {TOP_CONTRIBUTORS.map((user) => {
          return (
            <StyledEditLeader key={user.id}>
              <StyledAvatar>
                <a href="#">
                  <img
                    src={`https://image.tmdb.org/t/p/w50_and_h50_face${user.avatar_path}`}
                    alt={user.name}
                  />
                </a>
              </StyledAvatar>
              <StyledInfo>
                <p>
                  {user.count}
                  <br />
                  <a href="#">{user.name}</a>
                </p>
              </StyledInfo>
            </StyledEditLeader>
          );
        })}
        <p>
          <a href="#">View Edit History</a>
        </p>
      </StyledLeaders>
    </StyledSectionColumn>
  );
};
