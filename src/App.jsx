import "./App.css";

import BasicLayout from "./layouts/BasicLayout";
import { BrowserRouter } from "react-router-dom";
import Router from "./routes/Router";
import { ModalProvider } from "styled-react-modal";
function App() {
  return (
    <ModalProvider>
      <BrowserRouter>
        <BasicLayout>
          <Router />
        </BasicLayout>
      </BrowserRouter>
    </ModalProvider>
  );
}

export default App;
