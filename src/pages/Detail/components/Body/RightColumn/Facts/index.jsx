import { StyledSectionColumn } from "../index.styles";
import { StyledFact, StyledSocialLinks, ActionPopup } from "./index.styles";
import Visit from "./Visit";
import {
  FaFacebookSquare,
  FaTwitter,
  FaInstagram,
  FaLink,
} from "react-icons/fa";
import { useState } from "react";
export default ({ facts }) => {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  const languageNames = new Intl.DisplayNames(["en"], { type: "language" });
  return (
    <StyledSectionColumn>
      <StyledSocialLinks>
        <Visit message="Visit Facebook">
          <FaFacebookSquare size={26} />
        </Visit>
        <Visit message="Visit Twitter">
          <FaTwitter size={26} />
        </Visit>
        <Visit message="Visit Instagram">
          <FaInstagram size={26} />
        </Visit>
        <Visit message="Visit Homepage">
          <FaLink size={26} />
        </Visit>
      </StyledSocialLinks>
      <StyledFact>
        <strong>
          <bdi>Status</bdi>
        </strong>
        {facts.status}
      </StyledFact>
      <StyledFact>
        <strong>
          <bdi>Original Language</bdi>
        </strong>
        {languageNames.of(facts.original_language)}
      </StyledFact>
      <StyledFact>
        <strong>
          <bdi>Budget</bdi>
        </strong>
        {facts.budget ? formatter.format(facts.budget) : "-"}
      </StyledFact>
      <StyledFact>
        <strong>
          <bdi>Revenue</bdi>
        </strong>
        {facts.revenue ? formatter.format(facts.revenue) : "-"}
      </StyledFact>
    </StyledSectionColumn>
  );
};
