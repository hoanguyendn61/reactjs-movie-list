import {
  StyledKeywordsSection,
  StyledKeywordItem,
  StyledKeywordsList,
} from "./index.styles";
export default ({ keywords }) => {
  return (
    <StyledKeywordsSection>
      <h4>
        <bdi>Keywords</bdi>
      </h4>
      <StyledKeywordsList>
        {keywords.map((item) => {
          return (
            <StyledKeywordItem key={item.id}>
              <a href="#">{item.name}</a>
            </StyledKeywordItem>
          );
        })}
      </StyledKeywordsList>
    </StyledKeywordsSection>
  );
};
