import {
  StyledInfoPoster,
  StyledPosterExpand,
  StyledMetaWrapper,
  StyledForm,
  StyledTaggedPeopleWrapper,
} from "./index.styles";
import { GrFormClose } from "react-icons/gr";
import { AiFillLock } from "react-icons/ai";
import { RiCloseCircleFill } from "react-icons/ri";
import { FaPlus } from "react-icons/fa";
import { HiOutlineArrowRight } from "react-icons/hi";
export default ({ poster_path, onToggleModal }) => {
  return (
    <StyledPosterExpand>
      <div id="lightbox_popup">
        <a className="wrapper">
          <img
            loading="lazy"
            width={390}
            height={585}
            src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2/${poster_path}`}
          />
        </a>
        <StyledInfoPoster>
          <h3>
            <a className="close_right" onClick={onToggleModal}>
              <GrFormClose size={26} />
            </a>
          </h3>
          <StyledMetaWrapper>
            <h3>
              Info <AiFillLock />
            </h3>
            <StyledForm>
              <label className="flex">
                Primary? &nbsp;
                <RiCloseCircleFill />
              </label>
              <label>Added By</label>
              <p>
                <a>JoeSSS</a>
              </p>
              <label>Size</label>
              <p>
                <a>2000x3000</a>
              </p>
              <label>
                <span className="flex">Language</span>
                <div class="select-dropdown">
                  <select disabled>
                    <option value="en">English</option>
                  </select>
                </div>
              </label>
            </StyledForm>
            <StyledTaggedPeopleWrapper>
              <h3>
                Tagged People
                <a href="#">
                  <FaPlus />
                </a>
              </h3>
              <ul className="tagged_people">
                <li>No records have been added</li>
              </ul>
            </StyledTaggedPeopleWrapper>
            <div id="paging">
              <a className="next" href="#">
                <HiOutlineArrowRight />
              </a>
            </div>
          </StyledMetaWrapper>
        </StyledInfoPoster>
      </div>
    </StyledPosterExpand>
  );
};
