import {
  StyledSectionPanel,
  StyledMenu,
  StyledMenuAuto,
  StyledScrollerWrapper,
  StyledScroller,
  StyledBlur,
} from "../index.styles";
import MovieCard from "./MovieCard";
import { useState } from "react";
export default ({ recommendations }) => {
  const [scrollPosition, setScrollPosition] = useState(0);
  const onScroll = (e) => {
    const position = e.target.scrollLeft;
    setScrollPosition(position);
  };
  return (
    <StyledSectionPanel>
      <div>
        <StyledMenuAuto>Recommendations</StyledMenuAuto>
        {recommendations?.length > 0 ? (
          <StyledScrollerWrapper>
            <StyledScroller onScroll={onScroll}>
              {recommendations.map((item) => {
                return <MovieCard key={item.id} {...item} />;
              })}
            </StyledScroller>
            <StyledBlur hideFade={scrollPosition > 100 ? "1" : null} />
          </StyledScrollerWrapper>
        ) : (
          <p>
            We don't have enough data to suggest any movies based on this. You
            can help by rating movies you've seen.
          </p>
        )}
      </div>
    </StyledSectionPanel>
  );
};
