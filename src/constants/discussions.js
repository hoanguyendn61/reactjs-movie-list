const DISCUSSIONS = [
  {
    id: 1,
    topic: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    avatar_path: "/tz7DJXPNGNS6brlE2f8NU2iGC1g.jpg",
    status: "Open",
    replies: 3,
    last_reply: "JohnDoe",
    date_posted: "Sep 02, 2022 at 11:33 AM",
  },
  {
    id: 2,
    topic: "Nullam mattis leo sed rutrum volutpat.",
    avatar_path: "/fqZEvBX6Pm8d1Cg1JLULC0QNqjH.jpg",
    status: "Open",
    replies: 12,
    last_reply: "BobDoe",
    date_posted: "Sep 03, 2022 at 11:22 AM",
  },
  {
    id: 3,
    topic: "Aliquam aliquet porta neque in sodales.",
    avatar_path: "/qZW7TazXYrGysGBgO6ygeAaC4WO.jpg",
    status: "Open",
    replies: 6,
    last_reply: "JaneDoe",
    date_posted: "Sep 04, 2022 at 11:11 AM",
  },
];
export default DISCUSSIONS;
