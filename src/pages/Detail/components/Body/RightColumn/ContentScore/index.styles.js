import styled from "styled-components";

const StyledContentScore = styled.div`
  width: 100%;
  height: 38px;
  background: rgba(0, 0, 0, 0.2);
  border-radius: var(--imageBorderRadius);
  .true {
    background-color: #000;
    width: 80%;
    padding: 0 12px;
    display: flex;
    align-items: center;
    height: 100%;
    overflow: visible;
    border-radius: var(--imageBorderRadius);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    padding: 0 12px;
    p {
      color: #fff;
      font-weight: bold;
      font-size: 1em;
      padding: 0;
      margin-bottom: 0;
    }
  }
  + p {
    display: flex;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
    font-size: 0.9em;
    white-space: nowrap;
    box-sizing: border-box;
  }
`;

export { StyledContentScore };
