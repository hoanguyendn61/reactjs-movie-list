import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { selectMovies, fetchMovies } from "@/services/movies/moviesSlice";

export default function useMovie(pageNumber) {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.movies.loading);
  const movieStatus = useSelector((state) => state.movies.status);
  const moviesApi = useSelector(selectMovies);

  useEffect(() => {
    dispatch(fetchMovies(pageNumber));
  }, [pageNumber]);
  return { loading, movies: moviesApi.movies };
}
