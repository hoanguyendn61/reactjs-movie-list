import { StyledSectionColumn } from "../index.styles";
import { StyledContentScore } from "./index.styles";

export default () => {
  return (
    <StyledSectionColumn>
      <h4>Content Score</h4>
      <StyledContentScore>
        <div className="true">
          <p>88</p>
        </div>
      </StyledContentScore>
      <p>Almost there...</p>
    </StyledSectionColumn>
  );
};
