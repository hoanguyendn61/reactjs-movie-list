import { StyledLeftColumn } from "./index.styles";
import TopBilled from "./TopBilled";
import SocialPanel from "./SocialPanel";
import Recommendations from "./Recommendations";
import MediaPanel from "./MediaPanel";
import CollectionPanel from "./CollectionPanel";
export default ({ results }) => {
  return (
    <StyledLeftColumn>
      <TopBilled cast={results.credits.cast} />
      <SocialPanel reviews={results.reviews} />
      {results.belongs_to_collection && (
        <CollectionPanel collection={results.belongs_to_collection} />
      )}
      <MediaPanel
        videos={results.videos.results}
        backdrops={results.images.backdrops}
        posters={results.images.posters}
      />
      <Recommendations recommendations={results.recommendations.results} />
    </StyledLeftColumn>
  );
};
