import styled from "styled-components";

export const StyledHeaderInfo = styled.div`
  width: 100%;
`;
export const Tagline = styled.h3`
  margin-bottom: 0;
  font-size: 1.1em;
  font-weight: 400;
  font-style: italic;
  opacity: 0.7;
  width: 100%;
  margin: 0 0 8px 0;
`;
