import styled from "styled-components";

const StyledPopularityWaypoint = styled.div`
  width: 222px;
  height: 45px;
`;

export { StyledPopularityWaypoint };
