import styled from "styled-components";

export const StyledPosterWrapper = styled.div`
  border-width: 0px;
  min-width: 300px;
  width: 300px;
  height: 450px;
  overflow: hidden;
  border-radius: var(--imageBorderRadius);
`;

export const ImageContent = styled.div`
  width: 100%;
  min-width: 100%;
  height: 100%;
`;
export const ZoomContent = styled.div`
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.7);
  position: absolute;
  top: 0;
  left: 0;
  text-align: center;
  border-radius: var(--imageBorderRadius);
  visibility: hidden;
  opacity: 0;
  transition: linear 0.2s;
  backdrop-filter: blur(20px);
  a {
    color: #fff;
    width: 100%;
    height: 100%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-size: 1.3em;
  }
`;
export const Poster = styled.div`
  display: block;
  min-width: 300px;
  width: 300px;
  height: 450px;
  position: relative;
  top: 0;
  left: 0;
  img {
    display: block;
    width: 100%;
    min-width: 100%;
    height: 100%;
    min-height: 100%;
    border-width: 0px;
    outline: none;
  }
  &:hover {
    cursor: pointer;
    ${ZoomContent} {
      visibility: visible;
      opacity: 1;
    }
  }
`;
