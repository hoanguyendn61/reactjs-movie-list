import { StyledVideoCard } from "./index.styles";
import { FaPlay } from "react-icons/fa";
import VideoModal from "@/components/VideoModal";
import Modal from "styled-react-modal";
import { useState } from "react";
const StyledModal = Modal.styled`
  width: 1105px;
  height: 684px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #000;
`;
export default ({ videoKey, name }) => {
  const [isOpen, setOpen] = useState(false);
  const onToggleVideo = () => {
    setOpen((prevState) => !prevState);
  };

  return (
    <>
      <StyledVideoCard
        bg_image={`https://i.ytimg.com/vi/${videoKey}/hqdefault.jpg`}
      >
        <div className="wrapper">
          <a className="play_trailer" onClick={onToggleVideo}>
            <div className="play_background">
              <FaPlay size={21} />
            </div>
          </a>
          <StyledModal
            isOpen={isOpen}
            onEscapeKeydown={() => {}}
            onBackgroundClick={() => {}}
          >
            <VideoModal
              name={`${name}`}
              videoKey={videoKey}
              onToggleVideo={onToggleVideo}
            />
          </StyledModal>
        </div>
      </StyledVideoCard>
    </>
  );
};
