import { StyledSectionPanel } from "../index.styles";
import {
  StyledHeaderCollection,
  StyledCollectionWaypoint,
} from "./index.styles";
import { StyledRoundedButton } from "@/pages/Detail/components/Body/index.styles";
export default ({ collection }) => {
  return (
    <StyledSectionPanel>
      <StyledCollectionWaypoint>
        <StyledHeaderCollection backdrop_path={collection.backdrop_path}>
          <div>
            <h2>Part of the {collection.name}</h2>
          </div>
          <StyledRoundedButton>VIEW THE COLLECTION</StyledRoundedButton>
        </StyledHeaderCollection>
      </StyledCollectionWaypoint>
    </StyledSectionPanel>
  );
};
