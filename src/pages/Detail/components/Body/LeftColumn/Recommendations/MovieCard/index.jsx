import {
  StyledItemWrapper,
  StyledImageContent,
  StyledMovieFlex,
} from "./index.styles";
import { useState } from "react";
import { GoCalendar } from "react-icons/go";
import dateFormat from "dateformat";
export default ({ title, backdrop_path, vote_average, release_date }) => {
  const [isOpen, setIsOpen] = useState(false);
  const percentage = Math.round(vote_average * 10);
  return (
    <StyledItemWrapper>
      <StyledImageContent
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
        isVisible={isOpen}
      >
        <a href="#">
          <img
            src={`https://image.tmdb.org/t/p/w250_and_h141_face${backdrop_path}`}
            alt={title}
          />
        </a>
        <div className="meta">
          <span className="release_date">
            <GoCalendar /> &nbsp;&nbsp;
            {dateFormat(release_date, "mm/dd/yyyy")}
          </span>
          <span></span>
        </div>
      </StyledImageContent>
      <StyledMovieFlex>
        <a href="#" alt={title}>
          <bdi>{title}</bdi>
        </a>
        <span>{percentage}%</span>
      </StyledMovieFlex>
    </StyledItemWrapper>
  );
};
