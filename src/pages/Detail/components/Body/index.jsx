import {
  StyledBodyWrapper,
  StyledColumnWrapper,
  StyledContentWrapper,
} from "./index.styles";
import LeftColumn from "./LeftColumn";
import RightColumn from "./RightColumn";
export default ({ results }) => {
  const facts = {
    status: results.status,
    budget: results.budget,
    revenue: results.revenue,
    original_language: results.original_language,
  };
  const keywords = results.keywords.keywords;
  return (
    <StyledBodyWrapper>
      <StyledColumnWrapper>
        <StyledContentWrapper>
          <LeftColumn results={results} />
          <RightColumn facts={facts} keywords={keywords} />
        </StyledContentWrapper>
      </StyledColumnWrapper>
    </StyledBodyWrapper>
  );
};
