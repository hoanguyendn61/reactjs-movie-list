import { StyledCredits, StyledProfile } from "./index.styles";
import { filter, orderBy, groupBy, transform, chain } from "lodash";
export default ({ crew }) => {
  const transformedCrew = chain(crew)
    .filter((obj) => {
      const jobs = ["Director", "Screenplay", "Story", "Characters", "Writer"];
      return jobs.includes(obj.job);
    })
    .orderBy(["popularity"], ["desc"])
    .groupBy("name")
    .transform(function (result, val, key) {
      result.push({ [key]: val.map((i) => i.job).join(", ") });
    }, [])
    .value();
  return (
    <StyledCredits>
      {transformedCrew.map((item, index) => {
        const key = Object.keys(item)[0];
        return (
          <StyledProfile key={index}>
            <p>
              <a href="#">{key}</a>
            </p>
            <p className="character">{item[key]}</p>
          </StyledProfile>
        );
      })}
    </StyledCredits>
  );
};
