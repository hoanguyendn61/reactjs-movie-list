import { StyledSectionColumn } from "../index.styles";
import { StyledPopularityWaypoint } from "./index.styles";
import popularity_trend from "@/assets/images/popularity_trend.png";
export default () => {
  return (
    <StyledSectionColumn>
      <h4>Popularity Trend</h4>
      <StyledPopularityWaypoint>
        <img
          src={popularity_trend}
          alt="popularity_trend"
          height={50}
          width={203}
        />
      </StyledPopularityWaypoint>
    </StyledSectionColumn>
  );
};
