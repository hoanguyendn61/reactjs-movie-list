import ActorCard from "./ActorCard";
import { StyledSectionTopBilledCast, StyledCastScroller } from "./index.styles";
import { StyledAuto, StyledNewButton, StyledBlur } from "../index.styles";
import { FaArrowRight } from "react-icons/fa";
import { useState } from "react";
export default ({ cast }) => {
  const [scrollPosition, setScrollPosition] = useState(0);
  const onPeopleScroll = (e) => {
    const position = e.target.scrollLeft;
    setScrollPosition(position);
  };

  return (
    <StyledSectionTopBilledCast>
      <StyledAuto>Top Billed Cast</StyledAuto>
      <StyledCastScroller>
        <ol className="people scroller" onScroll={onPeopleScroll}>
          {cast.slice(0, 9).map((actor) => {
            return (
              <ActorCard
                key={actor.id}
                profile_path={actor.profile_path}
                name={actor.name}
                character={actor.character}
              />
            );
          })}
          <li className="filler view_more">
            <p>
              <a href="#">View More</a>&nbsp;
              <FaArrowRight
                style={{
                  verticalAlign: "text-top",
                }}
              />
            </p>
          </li>
        </ol>
        <StyledBlur hideFade={scrollPosition > 100 ? "1" : null} />
      </StyledCastScroller>
      <StyledNewButton>
        <a href="#">Full Cast & Crew</a>
      </StyledNewButton>
    </StyledSectionTopBilledCast>
  );
};
