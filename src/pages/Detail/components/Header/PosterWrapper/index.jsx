import {
  StyledPosterWrapper,
  Poster,
  ImageContent,
  ZoomContent,
} from "./index.styles";
import { useState } from "react";
import { HiArrowsExpand } from "react-icons/hi";
import Modal from "styled-react-modal";
import PosterExpand from "../PosterExpand";
const StyledModal = Modal.styled`
  width: 750px;
  height: 585px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #fff;
`;
export default ({ poster_path }) => {
  const [isOpen, setIsOpen] = useState(false);
  function toggleModal(e) {
    setIsOpen(!isOpen);
  }
  return (
    <StyledPosterWrapper>
      <Poster>
        <ImageContent>
          <img
            src={`https://image.tmdb.org/t/p/w300_and_h450_bestv2${poster_path}`}
          />
        </ImageContent>
        <ZoomContent>
          <a onClick={toggleModal}>
            <HiArrowsExpand />
            &nbsp;Expand
          </a>
          <StyledModal
            isOpen={isOpen}
            // onBackgroundClick={toggleModal}
            onEscapeKeydown={() => setIsOpen(false)}
          >
            <PosterExpand
              poster_path={poster_path}
              onToggleModal={toggleModal}
            />
          </StyledModal>
        </ZoomContent>
      </Poster>
    </StyledPosterWrapper>
  );
};
