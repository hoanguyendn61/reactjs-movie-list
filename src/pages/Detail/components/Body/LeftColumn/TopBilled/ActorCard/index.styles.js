import styled from "styled-components";

const StyledActorCard = styled.li`
  margin-top: 10px;
  margin-bottom: 10px;
  margin-left: 10px;
  margin-right: 4px;
  padding-bottom: 10px;
  border-radius: var(--imageBorderRadius);
  overflow: hidden;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
  border: 1px solid rgba(var(--lightGrey), 1);
  background-color: #fff;
`;
export { StyledActorCard };
