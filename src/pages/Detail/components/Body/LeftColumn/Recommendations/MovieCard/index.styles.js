import styled from "styled-components";

const StyledItemWrapper = styled.div`
  display: inline-block;
  float: none;
  width: 250px;
  margin: 0 15px 0 0;
`;
const StyledImageContent = styled.div`
  width: 250px;
  height: 141px;
  a {
    font-weight: 400;
    font-size: 1em;
  }
  img {
    top: 0;
    left: 0;
    width: 250px;
    height: 141px;
    border-radius: var(--imageBorderRadius);
  }
  .meta {
    visibility: ${(props) => (props.isVisible ? "visible" : "hidden")};
    background-color: rgba(255, 255, 255, 0.9);
    position: relative;
    top: -40px;
    left: 0;
    height: 40px;
    width: 100%;
    z-index: 1;
    box-sizing: border-box;
    padding-left: 10px;
    padding-right: 10px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    .release_date {
      display: flex;
      align-items: center;
      justify-content: center;
      svg {
        vertical-align: text-top;
      }
    }
  }
`;
const StyledMovieFlex = styled.p`
  display: flex;
  justify-content: space-between;
  margin-top: 4px;
  :last-child {
    margin-bottom: 0;
  }
  a {
    color: #000;
    font-weight: 400;
    font-size: 1em;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    padding-right: 20px;
  }
  span {
    padding-top: 0;
    margin-right: 0;
    display: inline-flex;
    align-items: center;
  }
`;
export { StyledItemWrapper, StyledImageContent, StyledMovieFlex };
