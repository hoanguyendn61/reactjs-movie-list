import styles from "./index.module.scss";
import { FaChevronRight, FaChevronDown } from "react-icons/fa";
import { useState } from "react";
import { SORT_DATA } from "@/constants";
export default () => {
  const [sortOpen, setSortOpen] = useState(false);
  const [filterOpen, setFilterOpen] = useState(false);
  const [whereToWatch, setWhereToWatch] = useState(false);
  const onHandleOpenSort = () => {
    setSortOpen(!sortOpen);
  };
  const onHandleOpenFilters = () => {
    setFilterOpen(!filterOpen);
  };
  const onHandleOpenWatch = () => {
    setWhereToWatch(!whereToWatch);
  };
  const closedClass = sortOpen ? "" : styles.closed;
  return (
    <div>
      <div className={`${styles.filter_panel} ${styles.card} `}>
        <div className={styles.name} onClick={onHandleOpenSort}>
          <h2>Sort</h2>
          {sortOpen ? (
            <FaChevronDown size={13} />
          ) : (
            <FaChevronRight size={13} />
          )}
        </div>
        <div className={`${styles.filter} ${closedClass}`}>
          <h3>Sort Results By</h3>
          {/* <select name="sort_by" id="sort_by">
            {SORT_DATA.map((item, index) => {
              return (
                <option key={index} value={item.value}>
                  {item.name}
                </option>
              );
            })}
          </select> */}
          <div class="select-dropdown">
            <select name="sort_by" id="sort_by">
              {SORT_DATA.map((item, index) => {
                return (
                  <option key={index} value={item.value}>
                    {item.name}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>
      <div
        className={`${styles.filter_panel} ${styles.card} `}
        style={{ marginTop: "12px" }}
      >
        <div className={styles.name} onClick={onHandleOpenFilters}>
          <h2>Filters</h2>
          {filterOpen ? (
            <FaChevronDown size={13} />
          ) : (
            <FaChevronRight size={13} />
          )}
        </div>
        <div className={`${styles.filter} ${styles.closed} `}></div>
      </div>
      <div
        className={`${styles.filter_panel} ${styles.card} `}
        style={{ marginTop: "12px" }}
      >
        <div className={styles.name} onClick={onHandleOpenWatch}>
          <h2>Where To Watch</h2>
          {whereToWatch ? (
            <FaChevronDown size={13} />
          ) : (
            <FaChevronRight size={13} />
          )}
        </div>
        <div className={`${styles.filter} ${styles.closed} `}></div>
      </div>
      <div className={styles.btn_search}>
        <p>
          <a href="#">Search</a>
        </p>
      </div>
    </div>
  );
};
