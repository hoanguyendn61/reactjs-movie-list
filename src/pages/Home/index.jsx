import styles from "./index.module.scss";
import MoviesList from "@/components/MoviesList";
import Filter from "@/components/Filter";
export default () => {
  return (
    <div className={styles.content_wrapper}>
      <div className={styles.title}>
        <h2>Popular Movies</h2>
      </div>
      <div className={styles.content}>
        <Filter />
        <MoviesList />
      </div>
    </div>
  );
};
