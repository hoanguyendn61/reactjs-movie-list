import { StyledPosterCard } from "../PostersTab/index.styles";
import { StyledBackdropCard } from "../BackdropsTab/index.styles";
import VideoCard from "../VideosTab/VideoCard";
export default ({ video, backdrop, poster }) => {
  return (
    <>
      {video && <VideoCard videoKey={video.key} name={video.name} />}
      <StyledBackdropCard>
        <img
          loading="lazy"
          src={`https://image.tmdb.org/t/p/w533_and_h300_bestv2${backdrop.file_path}`}
        />
      </StyledBackdropCard>
      <StyledPosterCard>
        <img
          loading="lazy"
          src={`https://image.tmdb.org/t/p/w220_and_h330_face${poster.file_path}`}
        />
      </StyledPosterCard>
    </>
  );
};
