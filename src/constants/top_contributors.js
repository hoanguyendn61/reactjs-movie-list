export const TOP_CONTRIBUTORS = [
  {
    avatar_path: "/yYG7Rhn9HfFpssIMeNiDynvxC14.jpg",
    id: "1",
    count: 751,
    name: "raze464",
  },
  {
    avatar_path: "/oTPjql0MGIwRrg9RolefYaaPlAN.jpg",
    id: "2",
    count: 64,
    name: "agnizheb",
  },
  {
    avatar_path: "/3nyT3aw7YK2fmP0egcxUY8sJVqQ.jpg",
    id: "3",
    count: 53,
    name: "Gus",
  },
  {
    avatar_path: "/nIS6URiCgAg1iplSCAyh24D2NCW.jpg",
    id: "4",
    count: 47,
    name: "Santiago",
  },
];
