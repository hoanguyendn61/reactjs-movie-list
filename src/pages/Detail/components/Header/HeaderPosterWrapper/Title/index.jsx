import { StyledTitle, OriginalTitle, Facts } from "./index.styles";
import { Link } from "react-router-dom";
import dateFormat from "dateformat";
import React from "react";
export default ({ id, title, release_date, genres, runtime }) => {
  const convertMinsToHrsMins = (mins) => {
    let h = Math.floor(mins / 60);
    let m = mins % 60;
    return `${h}h ${m}m`;
  };
  return (
    <StyledTitle>
      <OriginalTitle>
        <Link to={`/movie/${id}`}>{title}</Link>&nbsp;
        <span className="release_date">
          ({dateFormat(release_date, "yyyy")})
        </span>
      </OriginalTitle>
      <Facts>
        <span className="certification">PG-13</span>
        <span className="release">
          {dateFormat(release_date, "mm/dd/yyyy")} (US)
        </span>
        <span className="genres">
          {genres.map((genre) => {
            const isLast = genres.indexOf(genre) === genres.length - 1;
            return (
              <React.Fragment key={genre.id}>
                <a href="#">{genre.name}</a>
                {isLast ? "" : ", "}
              </React.Fragment>
            );
          })}
        </span>
        <span className="runtime">{convertMinsToHrsMins(runtime)}</span>
      </Facts>
    </StyledTitle>
  );
};
