import styled from "styled-components";

const StyledBackdropCard = styled.div`
  width: 533px;
  height: 300px;
  img {
    width: 533px;
    min-width: 533px;
    height: 300px;
    min-height: 300px;
  }
`;

export { StyledBackdropCard };
