import styled from "styled-components";

export const StyledHeaderPosterWrapper = styled.div`
  display: flex;
`;
export const HeaderSection = styled.section`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  align-content: center;
  box-sizing: border-box;
  padding-left: 40px;
  color: white;
`;
