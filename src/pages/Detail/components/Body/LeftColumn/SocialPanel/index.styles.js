import styled from "styled-components";

const StyledContent = styled.div`
  display: flex;
  width: 100%;
  border-radius: var(--imageBorderRadius);
  .original_content {
    width: 100%;
  }
`;

export { StyledContent };
