import styled from "styled-components";

export const StyledReviewContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  width: 100%;
  .content {
    display: flex;
    width: 100%;
    border-radius: var(--imageBorderRadius);
    .inner_content {
      width: 100%;
    }
  }
`;

export const StyledReviewCard = styled.div`
  width: 100%;
  padding: 20px;
  border: 1px solid rgba(var(--lightGrey), 1);
  border-radius: var(--imageBorderRadius);
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
  background-color: #fff;
`;
export const StyledGrouped = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  align-content: center;
  .avatar {
    width: 64px;
    margin-right: 20px;
    a {
      display: block;
      img.avatar {
        width: 64px;
        height: 64px;
        display: block;
        box-sizing: border-box;
        border-radius: 50%;
      }
    }
  }
`;
export const StyledTeaser = styled.div`
  padding-left: 84px;
  padding-top: 20px;
  box-sizing: border-box;
  div {
    max-height: 162px;
    overflow: auto;
    pre {
      white-space: pre-wrap;
      font-family: inherit;
    }
  }
  :before {
    clear: both;
    content: "";
    display: block;
  }
`;
export const StyledInfo = styled.div`
  width: 100%;

  .rating_wrapper {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    align-items: baseline;
    justify-content: flex-start;
  }
  h4 {
    font-weight: 300;
    font-size: 1.1em;
    margin-bottom: 8px;
    margin-top: 0;
  }
  a {
    color: #000;
  }
  h5 {
    font-weight: 300;
    font-size: 0.9em;
    margin-bottom: 0;
  }
`;
