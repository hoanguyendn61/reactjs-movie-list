import styled from "styled-components";

export const StyledHeader = styled.div`
  width: 100%;
  position: relative;
  z-index: 1;
  background-position: right -200px top;
  background-size: cover;
  background-repeat: no-repeat;
  background-color: #000;
  background-image: url(${(props) => props.backdropPoster});
`;
export const CustomBg = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  background-image: linear-gradient(
    to right,
    rgba(30, 30, 30, 1) 150px,
    rgba(30, 30, 30, 0.84) 100%
  );
`;
export const SingleColumn = styled.div`
  max-width: var(--maxPrimaryPageWidth);
  width: 100%;
  padding-top: 30px;
  padding-bottom: 30px;
  padding-left: 40px;
  padding-right: 40px;
  z-index: 0;
  box-sizing: border-box;
`;
export const OriginalHeader = styled.section`
  display: flex;
  flex-wrap: nowrap;
`;
