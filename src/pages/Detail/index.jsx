import Header from "./components/Header";
import Body from "./components/Body";
import Menu from "./components/Menu";
import {
  useGetMovieDetailsQuery,
  useGetMovieInfoByAppendQuery,
} from "@/api/themoviedbApi";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
export default () => {
  const { id } = useParams();
  const { data, error, isLoading } = useGetMovieInfoByAppendQuery({
    id,
    append: "credits,videos,images,recommendations,keywords,reviews",
  });
  const [results, setResults] = useState(null);
  useEffect(() => {
    if (!isLoading) {
      setResults(data);
    }
  }, [isLoading]);
  return (
    <>
      <Menu />
      {results && <Header results={results} />}
      {results && <Body results={results} />}
    </>
  );
};
