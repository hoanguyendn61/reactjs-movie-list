import styled from "styled-components";

const StyledPosterCard = styled.div`
  width: 200px;
  height: 300px;
  img {
    width: 200px;
    height: 300px;
  }
`;

export { StyledPosterCard };
