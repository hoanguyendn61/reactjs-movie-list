import React from "react";
import {
  StyledDropdown,
  StyledOpen,
  StyledItem,
  StyledCount,
} from "./index.styles";
import { GoTriangleRight } from "react-icons/go";
const Item = ({ children, submenu }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const iconRightTriangle = submenu ? <GoTriangleRight size={13} /> : null;
  return (
    <StyledItem
      onMouseEnter={() => {
        submenu && setIsOpen(true);
      }}
      onMouseLeave={() => {
        submenu && setIsOpen(false);
      }}
    >
      {children} {iconRightTriangle}
      {isOpen && (
        <StyledOpen submenu={true}>
          {submenu.map((item, index) => {
            if (item.count) {
              return (
                <Item key={index} submenu={item.children}>
                  {item.label} <StyledCount>{item.count}</StyledCount>
                </Item>
              );
            }
            return <Item key={index}>{item.label}</Item>;
          })}
        </StyledOpen>
      )}
    </StyledItem>
  );
};
const Dropdown = ({ children, overlay }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  return (
    <StyledDropdown
      onMouseEnter={() => setIsOpen(true)}
      onMouseLeave={() => setIsOpen(false)}
    >
      {children}
      {isOpen && (
        <StyledOpen>
          {overlay.map((item, index) => {
            return (
              <Item key={index} submenu={item.children}>
                {item.label}
                <StyledCount>{item.count}</StyledCount>
              </Item>
            );
          })}
        </StyledOpen>
      )}
    </StyledDropdown>
  );
};
export default Dropdown;
